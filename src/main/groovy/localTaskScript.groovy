import edu.wisc.doit.gradle.CompareVersions
import org.ajoberstar.grgit.Grgit

def grgit = Grgit.open(dir: args[0])
CompareVersions.compare(grgit.describe(), args[1])


