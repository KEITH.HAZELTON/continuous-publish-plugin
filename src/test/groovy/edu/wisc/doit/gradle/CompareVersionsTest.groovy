package edu.wisc.doit.gradle

import com.github.zafarkhaja.semver.Version
import org.gradle.api.GradleException
import org.junit.Test

import static org.junit.Assert.assertEquals

/**
 * Unit tests for {@link CompareVersions}.
 *
 * @author Nicholas Blair
 */
class CompareVersionsTest {

  @Test
  public void no_tags_yet() {
    CompareVersions.compare(null, "0.0.1-SNAPSHOT")
  }
  @Test
  public void whitespace() {
    CompareVersions.compare('   ', "0.0.1-SNAPSHOT")
  }
  @Test
  public void tag_exists_and_version_incremented() {
    CompareVersions.compare("0.0.1", "0.0.2-SNAPSHOT")
  }
  @Test(expected = GradleException.class)
  public void tag_exists_and_version_not_incremented_expects_failure() {
    CompareVersions.compare("0.0.1", "0.0.1")
  }
  @Test(expected = GradleException.class)
  public void describe_includes_hash_same_version_expects_failure() {
    CompareVersions.compare("0.20.7-1-gfbe61fd", "0.20.7")
  }

  @Test
  public void describe_maven_release_plugin_style_tags() {
    CompareVersions.compare("my-awesome-project-0.2.0-13-g98171dc", "0.2.1")
  }

  @Test
  void "parseVersionFromGitDescribe control"() {
    assertEquals(Version.valueOf("0.0.1"), CompareVersions.parseVersionFromGitDescribe("0.0.1"))
  }
  @Test
  void "parseVersionFromGitDescribe with preReleaseVersion"() {
    assertEquals(Version.valueOf("0.20.7"), CompareVersions.parseVersionFromGitDescribe("0.20.7-1-gfbe61fd"))
  }
  @Test
  void "parseVersionFromGitDescribe maven release plugin formatted tag"() {
    assertEquals(Version.valueOf("0.2.0"), CompareVersions.parseVersionFromGitDescribe("my-awesome-project-0.2.0-13-g98171dc"))
  }
}
