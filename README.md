# continuous-publish-plugin

This project provides a [Gradle](https://gradle.org/) plugin to help you continuously publish your project.

## Adding to your Gradle project

To get the dependency, you will need a `buildscript` block at the top of your build.gradle:

```
buildscript {
  repositories {
    maven () {
      url "https://code.doit.wisc.edu/maven/content/repositories/uw-releases"
      credentials {
        username codeDoitUsername
        password codeDoitPassword
      }
    }
  }
  dependencies {
    classpath 'edu.wisc.doit.gradle:continuous-publish-plugin:0.1.0'
  }
}
```

Note the `codeDoitUsername` and `codeDoitPassword` properties.

This project is distributed via the [Shared Tools Maven Artifact Repository](https://wiki.doit.wisc.edu/confluence/display/ST/Maven+Repository+Manager),
you will need a file (or symlink) named `gradle.properties` containing the following properties:

```
codeDoitUsername=your-netid-username
codeDoitPassword=your-code.doit-password-token
```

After that, apply the plugin like so:

```
apply plugin: `edu.wisc.doit.gradle.continuous-publish-plugin`
```

## Usage

This plugin provides tasks that are best intended to be invoked by a Continuous Integration environment like Jenkins.

### On Pull Requests

The `confirmProjectVersionIncremented` task is intended to be executed during a pull request to confirm that the contributor has incremented the `project.version` field in the project's `build.gradle` ahead of the current value in the file on the target branch.

If the merged result on build.gradle has `project.version` that is greater than what `git describe` returns, the build will be allowed to proceed. Otherwise, a GradleException is thrown to fail the build.

This behavior can be skipped by adding the following to your `build.gradle`:

```
cpublish {
  skip: true
}
```

### On commits to master branch

If a commit lands on master, and `cpublish.skip` is false, you can have confidence that the gradle `project.version` includes a new value. Running `gradle uploadArchives` to publish a new release will work.

After the `uploadArchives` task completes, you can have Jenkins tag the release and push the tag to the repository.

1. In your Jenkins build job, in the **Build** section, add a `Execute system Groovy script` step, and have it run a copy of the provided [artifact-version.groovy](artifact-version.groovy).
2. In the same job, in the **Post-build actions** section, add a `Git Publisher` step, and configure it:

![Git Publisher configuration screenshot](git-publisher-config.png)

## Developing the plugin

1. Java 8
2. Gradle *(optional, [Gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) is provided)*

## Building the plugin

> ./gradlew build

